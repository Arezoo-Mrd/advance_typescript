// *** problem 
const sampleObj = {
    a: 1,
    b: 2
}
// ********** type: (value: string) => any
const accsess = (str: string) => {
    return myObj[str];
}




// ?? 
// 1- Tighten the index
const myObj = {
    a: 1,
    b: 2
}

// ********** type: (value: keyof typeof myObj) => number
const accsess1 = (str: keyof typeof myObj) => {
    return myObj[str];
}



//  2- Loosen the object type
const myObj2: Record<string, number> = {
    a: 1,
    b: 2

}
// ********** type: (value:string) => number
const accsess2 = (str: string) => {
    return myObj2[str];
}


// 3- Cast the index
const myObj3 = {
    a: 1,
    b: 2
}

// ********** type: (value: string) => number
const accsess3 = (str: string) => {
    return myObj3[str as keyof typeof myObj3];
}
