// ?? Problem

const routes = {
    home: "/",
    admin: "/admin",
    users: "/users"
}
const gotoRoute = (route: '/' | '/admin' | '/users') => {
}

gotoRoute('/'); gotoRoute("/admin"); gotoRoute("/users") // ======> WTF
// gotoRoute(routes.admin) // ;(((
// or
routes.admin = "/whatever" // ====> NOOOOOOOOOOOOOOOOOOO

// !! Solution
//  **** as const


const routes1 = {
    home: "/",
    admin: "/admin",
    users: "/users"
} as const

const goToRoute = (route: '/' | '/admin' | '/users') => { }
goToRoute(routes1.admin) // ===> PERFECT :))))

routes1.admin = "/whatever" // ====> NICE *_*

// as const means READONLY


//  more example

const routes2 = Object.freeze({
    home: "/",
    admin: "/admin",
    users: "/users",
    deep: {
        whatever: "/deep/whatever"
    }
})

routes2.deep.whatever = "test" // =====> Object.freez work on low level :||||

//  BUT 
const routes3 = {
    home: "/",
    admin: "/admin",
    users: "/users",
    deep: {
        whatever: "/deep/whatever"
    }
} as const

routes3.deep.whatever = "/test" // ====> PERFECT :)))


//  OK
type TypeOfRoute = typeof routes1
// =====> hover on type

// *** Result
// type TypeOfRoute = {
//     readonly home: "/";
//     readonly admin: "/admin";
//     readonly users: "/users";
// }


type Routes = (typeof routes1)[keyof typeof routes1]
//  hover ====> type Routes = "/" | "/admin" | "/users"
type RouteKeys = keyof typeof routes1

type Routes2 = typeof routes1[RouteKeys]

// so

const goToRoutes1 = (route: Routes) => { }
goToRoutes1("/");
goToRoutes1("/admin")
goToRoutes1("/users")