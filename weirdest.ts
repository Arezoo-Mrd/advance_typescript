
// ?? PROBLEM

const emptyObj: {} = false

const emptyObj2: {} = null
const emptyObj3: {} = undefined

const emptyObj4: {} = 'string'
const emptyObj5: {} = 123
const emptyObj6: {} = {
    foo: 'bar'
}


//  more example
const receivesEmpty = (empty: {}) => { }

receivesEmpty("test") // =====> WHy? this is bad !!!
receivesEmpty(123)
receivesEmpty({
    foo: "bar"
})


// !! SOLUTION
const receivesEmpty1 = (empty: Record<PropertyKey, never>) => { }
receivesEmpty1("test") // ===> perfect
receivesEmpty1(123)
receivesEmpty1({ foo: 'bar' })


receivesEmpty1({})


// USE CASE
type MyimaginaryType = {} | null | undefined // instance of ===> string | number | boolean | ...

const example1: MyimaginaryType = "str"
const example2: MyimaginaryType = 123
const example3: MyimaginaryType = true
const example4: MyimaginaryType = null
const example5: MyimaginaryType = undefined
const example6: MyimaginaryType = { foo: "bar" }

type MyimaginaryType2 = unknown | string


// 2-
declare const maybeString: undefined | string;
maybeString.length // ====> error

declare const maybeString2: null | string;
maybeString2.length // ====> error


type Unknownish = {}

const logData = (data: Unknownish) => {
    if ("foo" in data && typeof data.foo === "string") {
        console.log(data.foo)
    }
}


